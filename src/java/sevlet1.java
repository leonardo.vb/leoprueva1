/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leona
 */
@WebServlet(urlPatterns = {"/sevlet1"})
public class sevlet1 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
                
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
              ResultSet rs = null;
           Connection conexion=null;
           String ids=request.getParameter("codigo");
           String nombre=request.getParameter("nombre");
           String apellido=request.getParameter("apellido");
           String telefono=request.getParameter("telefono");
           //Leemos el driver de Mysql
           try{Class.forName("com.mysql.jdbc.Driver");
           //Se ontine una conexion con la base de datos.
           conexion = DriverManager.getConnection("jdbc:mysql://localhost/guia71","root","");
           Statement s = conexion.createStatement();
           s.executeUpdate("Insert into empleados values("+ids+",\""+nombre+"\",\""+apellido+"\",\""+telefono+"\")");
           rs = s.executeQuery("select * from empleados");
           //Decimos que nos hemos conectado
            out.println("<html>");
            out.println("<body>");
            out.println("<h1>Datos ingresados Exitosamente</h1>");
            out.println("<table aling='center' with='75%'border=1>");
            out.println("<tr><th>Codigo</th><th>Nombres</th><th>Apellidos"+"</th><th>Telefono</th></tr>");
            while (rs.next()){
             out.println("<tr><td>"+rs.getInt("Codigo")+"</td><td>"+rs.getString("Telefono")+"</td></tr>");
            }
            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
            
            conexion.close();
           }
           catch(ClassNotFoundException e1){
               //Error si no pudo leer el drive
               out.println("ERROR:No encuentro el drive de la BD:"+e1.getMessage());
           }
           catch(SQLException e2){
               //Error SQL: login/passwd mal
               out.println("ERROR:Fallo en SQL:"+e2.getMessage());
           }
           finally{
               out.close();
           }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
